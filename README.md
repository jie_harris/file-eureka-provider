# Vue-simple-uploader

#### 介绍
Vue+Spring boot文件上传下载功能模块
支持多文件上传、分块上传、进度条显示、秒传、上传暂停、断点续传功能
上传下载组件使用第三方组件vue-simple-uploader


#### 软件架构
前端使用Vue，后端使用Spring Boot
前后端在同一项目中，vue：src/main/protal；java：src/main/java

#### 安装教程

1. 在protal目录先npm install或cnpm install（需要node环境）
2. npm install axios --save

#### 使用说明

1. FileEurekaProviderApplication.java为后端spring boot启动类
2. 通过npm run dev启动vue前端后运行java启动类即可访问页面
3.第三方组件[vue-simple-uploader](/https://github.com/simple-uploader/Uploader)

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)