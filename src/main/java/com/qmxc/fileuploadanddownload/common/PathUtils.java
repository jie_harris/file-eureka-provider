package com.qmxc.fileuploadanddownload.common;

import org.springframework.util.ClassUtils;

import java.io.File;

/**
 * @author He Changjie on 2018/12/4
 * 路径工具类
 * 功能：
 *      1、获取存放服务器文件列表的资源文件路径
 *      2、获取服务器存放文件的目录路径
 */
public class PathUtils {
    private static String propertiesFileName="filelist.properties";

    /**
     * 获取存放服务器文件列表的资源文件路径
     * @return 资源文件路径(String)
     */
    public static String getPropertiesPath(){
        StringBuffer sb=new StringBuffer();
        sb.append(ClassUtils.getDefaultClassLoader().getResource("").getPath().substring(1));
        sb.append("static/").append(PathUtils.propertiesFileName);
        File file=new File(sb.toString());
        if(!file.exists()){
            file.mkdirs();
        }
        return sb.toString();
    }

    /**
     * 获取服务器存放文件的目录路径
     * @return 目录路径（String)
     */
    public static String getFileDir(){
        String path=ClassUtils.getDefaultClassLoader().getResource("").getPath().substring(1)+"static/file";
        File dir=new File(path);
        if(!dir.exists()){
            dir.mkdirs();
        }
        return path;
    }
}
