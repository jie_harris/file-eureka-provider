package com.qmxc.fileuploadanddownload.common;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

/**
 * @author He Changjie on 2018/11/29
 * @description:
 * PropertiesUtils工具类
 * 功能：1、向文件列表中存储信息（文件名：文件路径）
 *       2、获取文件列表中的全部信息
 *       3、获得文件扩展名对应的Content-Type(Mime-Type)
 *       4、根据文件名获取文件路径
 *       5、判断文件列表是否已经含有该文件
 */
public class PropertiesUtils {

    /**
     * 向文件列表中存储信息（文件名：文件路径）
     * @param fileName 文件名
     * @param filePath 文件路径
     * @return 是否保存到properties文件成功
     */
    public static boolean setProperty(String fileName,String filePath){
        Properties prop = new Properties();
        try(
                FileOutputStream oFile=new FileOutputStream(PathUtils.getPropertiesPath(), true);
                ) {
            prop.setProperty(fileName, filePath);
            prop.store(new OutputStreamWriter(oFile, StandardCharsets.UTF_8), null);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
        return true;
    }

    /**
     * 获取文件列表中的全部信息
     * @return 文件列表中的全部信息，map集合
     */
    public static Map<String,String> getFileList(){
        Properties prop = new Properties();
        Map<String,String> map=new HashMap<>();
        String filePath=PathUtils.getPropertiesPath();
        try(BufferedInputStream in = new BufferedInputStream(new FileInputStream(filePath));
        ) {
            prop.load(new InputStreamReader(in, StandardCharsets.UTF_8));
            for (String key : prop.stringPropertyNames()) {
                if(key.length()>1){
                    map.put(key,prop.getProperty(key));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;
    }

    /**
     * 根据文件名获取文件路径
     * @param fileName 文件名
     * @return 文件路径
     */
    public static String getFilePath(String fileName){
        Properties prop = new Properties();
        try(BufferedInputStream in = new BufferedInputStream(new FileInputStream(PathUtils.getPropertiesPath()));
        ) {
            prop.load(new InputStreamReader(in, StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  prop.getProperty(fileName);
    }

    /**
     * 判断文件列表是否已经含有该文件
     * @param key 文件名
     * @return true为已存在
     */
    public static boolean contains(String key){
        String s = getFilePath(key);
        return s != null && !"".equals(s) ;
    }

    /**
     * 获得文件扩展名对应的Content-Type(Mime-Type)
     * @param key 文件扩展名
     * @return 返回对应的Content-Type(Mime-Type)
     */
    public static String getContentType(String key){
        if(key.lastIndexOf(".")!=-1){
            key=key.substring(key.lastIndexOf("."));
        }
        ResourceBundle bundle = ResourceBundle.getBundle("contentType");
        if(!bundle.containsKey(key)){
            return "application/octet-stream";
        }
        return bundle.getString(key);
    }
}
