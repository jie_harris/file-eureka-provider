package com.qmxc.fileuploadanddownload.controller;

import com.qmxc.fileuploadanddownload.entity.Chunk;
import com.qmxc.fileuploadanddownload.services.FileService;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author He Changjie on 2018/11/29
 * 文件Controller
 * 处理文件的上传与下载
 */
@RestController
public class FileController {

    @Resource
    FileService fileService;

    /**
     * 返回文件列表
     * @param response 响应
     * @return 文件列表map
     */
    @RequestMapping("/fileList")
    public Map<String,String> filelist(HttpServletResponse response){
        return fileService.getFileList();
    }

    /**
     * 文件下载
     * @param name 文件名（页面传递过来的参数)
     * @param request 请求
     * @param response 响应
     */
    @RequestMapping("/download")
    public void down(@RequestParam("name") String name, HttpServletRequest request, HttpServletResponse response) {
        fileService.fileDownload(name,request,response);
    }


    /**
     * 处理文件上传POST请求
     * 将上传的文件存放到服务器内
     * @param chunk 文件块
     * @param response 响应
     * @return 上传响应状态
     */
    @PostMapping("/fileUpload")
    public String uploadPost(@ModelAttribute Chunk chunk, HttpServletResponse response){
       return fileService.fileUploadPost(chunk,response);
    }

    /**
     * 处理文件上传GET请求
     * 验证上传的文件块，是否允许浏览器再次发送POST请求（携带二进制文件的请求流，FormData）
     * @param chunk 文件块
     * @param response 响应
     * @return 文件块
     */
    @GetMapping("/fileUpload")
    public void uploadGet(@ModelAttribute Chunk chunk,HttpServletResponse response){
        fileService.fileUploadGet(chunk,response);
    }
}
