package com.qmxc.fileuploadanddownload.interfaces;

import com.qmxc.fileuploadanddownload.entity.Chunk;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * @author He Changjie on 2018/12/4
 */
public interface FileServiceInterface {

    /**
     * 获取文件列表
     * @return 文件列表map
     */
    public Map<String,String> getFileList();

    /**
     * 文件下载业务
     * 根据文件名查找文件列表对应文件的路径，将其写到response的输出流
     * @param fileName 文件名
     * @param request 请求
     * @param response 响应
     */
    public void fileDownload(String fileName, HttpServletRequest request, HttpServletResponse response);

    /**
     * 处理POST请求（携带文件流）
     * @param chunk 文件块
     * @param response 响应
     */
    public String fileUploadPost(Chunk chunk, HttpServletResponse response);

    /**
     * 处理GET请求（携带文件流）
     * @param chunk 文件块
     * @param response 响应
     * @return 文件块
     */
    public void fileUploadGet(Chunk chunk, HttpServletResponse response);
}
