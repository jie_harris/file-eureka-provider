import Vue from 'vue'
import Router from 'vue-router'
import FileUpload from '@/components/FileUpload'
import FileList from '@/components/FileList'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect:'fileupload'
    },
    {
      path: '/fileupload',
      name: 'FileUpload',
      component: FileUpload
    },
    {
      path:'/filedownload',
      name:FileList,
      component:FileList
    }
  ]
})
